import React, { useState, useEffect } from 'react';


function ShoesForm(props) {
    const [modelName, setModelName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [color, setColor] = useState('');
    const [image, setImage] = useState('');
    const [bins, setBins] = useState([]);
    const [bin, setBin] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleBinChange = (e) => {
        const value = e.target.value;
        setBin(value);
    }

    const handlemodelNameChange = (e) => {
        const value = e.target.value;
        setModelName(value);
    }

    const handleManufacturerChange = (e) => {
        const value = e.target.value;
        setManufacturer(value);
    }

    const handleColorChange = (e) => {
        const value = e.target.value;
        setColor(value);
    }

    const handleImageChange = (e) => {
        const value = e.target.value;
        setImage(value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};

        data.model_name = modelName;
        data.manufacturer = manufacturer;
        data.color = color;
        data.image = image;
        data.bin = bin;

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application.json',
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);

        if (response.ok) {
            const newShoe = await response.json();

            setModelName('');
            setManufacturer('');
            setColor('');
            setImage('');
        }

    }



    let dropdownClasses = 'form-select d-none';
    let spinnerClasses = 'd-flex justify-content-center mb-3';
    if (bins.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    return (
        <div className="container">
            <div className="row">
                <div className="col col-sm-auto">
                    <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="https://i.pinimg.com/originals/f7/19/8b/f7198ba78949c77169be3b838dfc226c.jpg" />
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-shoe-form">
                                <h1 className="card-title">Register a new Shoe</h1>
                                <div className={spinnerClasses} id="loading-conference-spinner">
                                    <div className="spinner-grow text-secondary" role="status">
                                        <span className="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <select onChange={handleBinChange} value={bin} name="closet" id="closet" className={dropdownClasses} required>
                                        <option value="">Choose a closet</option>
                                        {bins.map(bin => {
                                            return (
                                                <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
                                                )
                                              })}
                                    </select>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handlemodelNameChange} value={modelName} required placeholder="Model name" type="text" id="name" name="name" className="form-control" />
                                            <label htmlFor="name">Model name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleManufacturerChange} value={manufacturer} required placeholder="Manufacturer" type="text" id="name" name="name" className="form-control" />
                                            <label htmlFor="name">Manufacturer</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleColorChange} value={color} required placeholder="Color" type="text" id="name" name="name" className="form-control" />
                                            <label htmlFor="name">Color</label>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleImageChange} value={image} required placeholder="Image URL" type="text" id="name" name="name" className="form-control" />
                                            <label htmlFor="name">Image URL</label>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="mb-3">
                                        <button className="btn btn-lg btn-primary">Register!</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ShoesForm;
