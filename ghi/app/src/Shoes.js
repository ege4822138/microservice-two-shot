import React, { useState, useEffect } from 'react';


function ShoesList(props) {
    const [shoes, setShoe] = useState([]);

    async function getShoes() {
        const response = await fetch("http://localhost:8080/api/shoes/");
        if (response.ok) {
            const { shoes } = await response.json();
            setShoe(shoes);
        } else {
            console.error('An error occured while fetching the data.')
        }

    }
    useEffect(()=>{
        getShoes();
    }, [])

    const handleDelete=((id)=>{
        if (window.confirm("Do you want to remove?")) {
            fetch("http://localhost:8080/api/shoes/"+id,
            {method:"DELETE"}).then(()=>{

                window.location.reload();

            }).catch((err)=>{
                console.log(err.message)
            })
        }
    })

    if (shoes === undefined) {
        return null;
    }

   return (
        <>
        <h1>List of Shoes</h1>
        <table className="table table-striped">
          <thead>
            <tr className="table-secondary">
              <th scope="col">Model</th>
              <th scope="col">Manufacturer</th>
              <th scope="col">Color</th>
              <th scope="col">Closet</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            { shoes.map(shoe => {
              return (
              <tr key={shoe.id}>
                <td>{shoe.model_name}</td>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.color}</td>
                <td>{shoe.bin.closet_name}</td>
                <td><button className="btn btn-sm btn-outline-danger" onClick={()=>handleDelete(shoe.id)}>Delete</button></td>
              </tr>
            );
            })}
          </tbody>
        </table>
        </>
  );
}

export default ShoesList;
