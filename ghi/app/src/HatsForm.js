import React, {useEffect, useState} from 'react';

function HatsForm(){

    const [name, setName] = useState('');
    const [fabric, setFabric] = useState('');
    const [color, setColor] = useState('');
    const [Url, setUrl] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleUrlChange = (event) => {
        const value = event.target.value;
        setUrl(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.fabric = fabric;
        data.color = color;
        data.picture_URL = Url;
        data.location = location;

        const hatUrl = "http://localhost:8090/hats/";

        const fetchConfig = {
            method:"post",
            body:JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if(response.ok){
            const newHat = await response.json();
            console.log(newHat);

            setName('');
            setFabric('');
            setColor('');
            setUrl('');
            setLocation('');
        }
    }


    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations";
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setLocations(data.locations)
        };
    };

    useEffect(() => {
        fetchData();
    }, []);


    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="Color" type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleUrlChange} value={Url} placeholder="Url of photo image" required type="text" name="URL" id="URL" className="form-control" />
                <label htmlFor="URL">Url of photo image</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option key={location.href} value={location.href}>
                            {location.closet_name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>

    )
}

export default HatsForm
