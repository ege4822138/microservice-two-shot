import React, { useEffect, useState } from 'react';

function HatsList() {
  const [hats, setHats] = useState([],[],[]);

  const fetchData = async () => {
    const url = "http://localhost:8090/hats/";

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setHats(data.hats)
        console.log(data.hats)
      }
    } catch (e) {
      console.error(e);
    }
  };

  const deleteHat = async (id) => {
    const fetchconfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json"
      },
    };

    const response = await fetch(`http://localhost:8090/hats/${id}`, fetchconfig);
    if (response.ok) {
      fetchData();
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
   <div className="container">
    <div className="row">
          {hats.map((hat) => {
            return (
              <div className="col-md-6  mb-3 d-flex justify-content-center">
            <div key={hat.id} className="col mb-3 shadow" style={{background: "black"}}>
                <div className="p-3">
                  <div className="text-center">
                    <img src={hat.picture_URL} className="rounded" style={{width:"200px", height:"200px"}} />
                  </div>
              <div className="card-body">
                <h4 className='card-title' style={{color: "white"}}>{hat.name}</h4>
                <h5 className="card-text mb-2" style={{color: "white"}}>{hat.fabric}</h5>
                <p className="card-text" style={{color: "white"}}>
                {hat.color}
                </p>
                </div>
                <div className="card-footer">
                <p className="card-text" style={{color: "white"}}>Section Number: {hat.location.section_number} </p>
                <p className="card-text" style={{color: "white"}}>Shelf Number: {hat.location.shelf_number} </p>
                </div>
                <div className="text-center mb-3">
                  <button onClick={() => deleteHat(hat.id)} className="btn btn-danger">Delete</button>
                </div>
                </div>
                </div>
                </div>
            );
        })}
    </div>
    </div>
  );
}

export default HatsList;



// ************************************

// import React, { useEffect, useState } from 'react';

// function HatColumn(props){

//   return (
//     <div className="col">
//           {props.list.map((hat) => {
//             return (
//             <div key={hat.id} className="card mb-3 shadow" >
//               <img src={hat.picture_URL} className="card-img-top" />
//               <div className="card-body">
//                 <h5 className='card-title'>{hat.name}</h5>
//                 <h6 className="card-subtitle mb-2 text-muted">
//                 {hat.fabric}
//                 </h6>
//                 <p className="card-text">
//                 {hat.color}
//                 </p>
//                 </div>
//                 <div className="card-footer">
//                   {hat.location && (
//                      <>
//                       <p className="card-text">Section Number: {hat.location.section_number} </p>
//                       <p className="card-text">Shelf Number: {hat.location.shelf_number} </p>
//                      </>
//                   )}
//                 </div>
//                 <div>
//                   <button onClick={() => props.deleteHat(hat.id)} className="btn btn-danger">Delete</button>
//                 </div>
//             </div>
//             );
//         })}
//     </div>
//   );
// }


// function HatsList() {
//   const [hats, setHats] = useState([]);

//   const fetchData = async () => {
//     const url = "http://localhost:8090/hats/";

//     try {
//       const response = await fetch(url);
//       if (response.ok) {
//         const data = await response.json();

//         const request = data.hats.map((hat) =>
//           fetch(`http://localhost:8090/hats/${hat.id}`)
//         )

//         const responses = await Promise.all(request)

//         const hatDetails = await Promise.all(
//           responses.map((hatResponse) => hatResponse.json())
//         );

//         setHats(hatDetails)

//       }
//     } catch (e) {
//       console.error(e);
//     }
//   };


//   const deleteHat = async (id) => {
//     const fetchconfig = {
//       method: "DELETE",
//       headers: {
//         "Content-Type": "application/json"
//       },
//     };

//     const response = await fetch(`http://localhost:8090/hats/${id}`, fetchconfig);
//     if (response.ok) {
//       fetchData();
//     }
//   };

//   useEffect(() => {
//     fetchData();
//   }, []);

//   return(
//     <>
//       <div className="container">
//         <h2>Hats</h2>
//         <div className="row">
//           {hats.map((hatList, index) => {
//             return (
//               <HatColumn list={hats} deleteHat={deleteHat} />
//             );
//           })}
//         </div>
//       </div>

//     </>
//   )
// }

// export default HatsList;
