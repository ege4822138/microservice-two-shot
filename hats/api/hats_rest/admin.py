from django.contrib import admin
from .models import Hat, LocationVO

@admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "name",
        "location",
    ]

@admin.register(LocationVO)
class LocationVOAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "closet_name",
        "section_number",
        "shelf_number",
    ]
