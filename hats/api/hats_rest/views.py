from django.shortcuts import render
from django.http import JsonResponse
from .models import Hat, LocationVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "name", "fabric", "color", "location", "picture_URL"]

    def get_extra_data(self, o):
        return {"location": {"id": o.location.id,
                             "closet_name": o.location.closet_name,
                             "section_number": o.location.section_number,
                             "import_href": o.location.import_href,
                             "shelf_number": o.location.shelf_number,
                             }}


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "fabric",
        "color",
        "picture_URL",
    ]

    encoders = {"location": LocationVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
                            {"hats": hats},
                            encoder=HatListEncoder,
                            safe=False
                            )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)

            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def show_hat(request, id):
    if request.method == "GET":
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        try:
            if "location" in content:
                location = LocationVO.objects.get(id=content["location"])
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

    Hat.objects.filter(id=id).update(**content)
    hat = Hat.objects.get(id=id)
    return JsonResponse(
        hat,
        encoder=HatDetailEncoder,
        safe=False,
    )


        # if location_vo_id is not None:
        #     hats = Hat.objects.filter(location=location_vo_id)
        # else:
        #     hats = Hat.objects.all()
