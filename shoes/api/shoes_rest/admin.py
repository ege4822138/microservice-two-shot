from django.contrib import admin
from .models import Shoes

@admin.register(Shoes)
class ShoesAdmin(admin.ModelAdmin):
    list_display = [
        "manufacturer",
        "model_name",
        "color",
        "bin",
    ]
