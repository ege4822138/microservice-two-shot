from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Shoes, BinVO
from common.json import ModelEncoder
from django.http import JsonResponse
import json


class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "model_name",
        "manufacturer",
        "color",
        "image",
        "bin"
    ]

    def get_extra_data(self, o):
        return {"bin": {
                        "href": o.bin.import_href,
                        "closet_name": o.bin.closet_name,
                        "bin_number": o.bin.bin_number,
                        "bin_size": o.bin.bin_size
                        }}




class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
        ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "model_name",
        "color",
        "manufacturer",
        "image",
    ]

    encoders = {"bin": BinVODetailEncoder()}




@require_http_methods(["GET", "POST"])
def api_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoe = Shoes.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoes.objects.all()

        return JsonResponse({"shoes" : shoes }, encoder=ShoeListEncoder, safe=False)

    else:     #POST
        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin

        except BinVO.DoesNotExist:
            return JsonResponse( {"message": "invalid bin id"}, status=400)


        shoe = Shoes.objects.create(**content)
        return JsonResponse(shoe, encoder=ShoeListEncoder, safe=False)

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoes.objects.get(id=id)
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)


    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else: #PUT
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin = BinVO.objects.get(id=content["href"])
                content["href"] = bin

        except BinVO.DoesNotExist:
            return JsonResponse( {"message": "invalid bin id"}, status=400)

    Shoes.objects.filter(id=id).update(**content)
    shoe = Shoes.objects.get(id=id)
    return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)
